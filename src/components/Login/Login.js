import React, { useState, useEffect } from 'react';

function Login(props) {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');

  const nameChangeHandler = e => {
    const name = e.target.value;
    setName(name);
  };

  const passChangeHandler = e => {
    const pass = e.target.value;
    setPassword(pass);
  };

  const submitHandler = e => {
    const nameValue = name.trim();
    const passwordValue = password.trim();
    localStorage.setItem('logeedName', nameValue);
    localStorage.setItem('logeedPassWord', passwordValue);
    if (nameValue && passwordValue) {
      props.history.push(
        '/RestaurantDetails/' + props.match.params.res_id + '/Review'
      );
    }
    e.preventDefault();
  };
  return (
    <div>
      <div className='container'>
        <h3>Login Here</h3>
        <form onSubmit={submitHandler}>
          <div className='form-group'>
            <label>Name:</label>
            <input
              required
              type='text'
              className='form-control'
              id='name'
              value={name}
              onChange={nameChangeHandler}
              placeholder='Enter name'
              name='name'
            />
          </div>
          <div className='form-group'>
            <label>Password:</label>
            <input
              required
              type='password'
              className='form-control'
              id='pwd'
              value={password}
              onChange={passChangeHandler}
              placeholder='Enter password'
              name='pswd'
            />
          </div>
          <button type='submit' className='btn btn-primary'>
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}

export default Login;
