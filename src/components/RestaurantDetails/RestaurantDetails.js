import React, { Component } from 'react';
import axios from 'axios';
import './RestaurantDetails.css';
import { Link } from 'react-router-dom';

import img1 from '../../assets/img1.jpg'
import img2 from '../../assets/img2.jpg'
import img3 from '../../assets/img3.jpg'
import img4 from '../../assets/img4.jpg'
import img5 from '../../assets/img5.jpg'

const images=[
  img1,img2,img3,img4,img5
]

class RestaurantDetails extends Component {
  constructor() {
    super();
    this.state = {
      restaurantDetails: {},
      ratings: '',
      highlights: []
    };
  }

  componentDidMount() {
    const url_query_params = this.props.match.params.res_id;
    axios
      .get('https://developers.zomato.com/api/v2.1/restaurant', {
        headers: { 'user-key': '4e02eb9035ae93ecffda5c28850e6442' },
        params: { res_id: url_query_params }
      })
      .then(response => {
        this.setState({ restaurantDetails: { ...response.data } });
        this.setState({ ratings: response.data.user_rating.aggregate_rating });
        this.setState({ highlights: response.data.highlights });
        console.log('restaurantDetails', this.state.restaurantDetails);
      });
  }
  render() {
    return (
      <div className='container'>
        <div className='navOuter'>
          <div className='navTab'>
            <ul>
              <button className='active navButton btn btn-primary'>
                <Link
                  to={
                    '/RestaurantDetails/' +
                    this.props.match.params.res_id +
                    '/Login'
                  }
                >
                  Reviews
                </Link>
              </button>
              <button className='active btn btn-primary'>
                <Link
                  to={
                    '/RestaurantDetails/' +
                    this.props.match.params.res_id +
                    '/Login'
                  }
                >
                  Photos
                </Link>
              </button>
            </ul>
          </div>
        </div>
        <div className='outerContainer'>
          <div className='img-div'>
            {this.state.restaurantDetails.featured_image == '' ? (
              <img
                className='featured_image'
                src={images[Math.floor(Math.random() * images.length)]}
                alt='collection'
              />
            ) : this.state.restaurantDetails.hasOwnProperty('featured_image') ==
              false ? (
              <img
                className='featured_image'
                src={images[Math.floor(Math.random() * images.length)]}
                alt='collection'
              />
            ) : (
              <img
                className='featured_image'
                src={this.state.restaurantDetails.featured_image}
              />
            )}
          </div>
          <div className='info-div'>
            <h5>Name- </h5>
            <h6 className='heiglights'>
              {this.state.restaurantDetails.name == ''
                ? ''
                : this.state.restaurantDetails.name}
            </h6>
            <h5>Cuisines- </h5>
            <h6 className='heiglights'>
              {this.state.restaurantDetails.cuisines == ''
                ? ''
                : this.state.restaurantDetails.cuisines}
            </h6>
            <h5>Timing- </h5>
            <h6 className='heiglights'>
              {this.state.restaurantDetails.timings == ''
                ? ''
                : this.state.restaurantDetails.timings}
            </h6>

            <h5>Avg-Cost- </h5>
            <h6 className='heiglights'>
              {this.state.restaurantDetails.average_cost_for_two == ''
                ? ''
                : this.state.restaurantDetails.average_cost_for_two}{' '}
              Rs.
            </h6>
            <h5>Ratings- </h5>
            <h6 className='heiglights'>
              {this.state.ratings == '' ? '' : this.state.ratings}
            </h6>
            <h5>Phone Number- </h5>
            <h6 className='heiglights'>
              {this.state.restaurantDetails.phone_numbers == ''
                ? ''
                : this.state.restaurantDetails.phone_numbers}
            </h6>
            <h5>Highlights- </h5>
            <ul className='heiglights'>
              {' '}
              {this.state.highlights.length == 0
                ? ''
                : this.state.highlights.map((e, key) => {
                    return (
                      <li key={key} value={e}>
                        {e}
                      </li>
                    );
                  })}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
export default RestaurantDetails;
