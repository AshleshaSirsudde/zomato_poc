import React, { Component } from 'react';
import axios from 'axios';

class Search extends Component {
  constructor() {
    super();
    this.state = {
      inputCity: 'Pune',
      locationData: {
        cityID: '',
        entityID: '',
        entityType: ''
      }
    };
    this.changeCityHandler = this.changeCityHandler.bind(this);
  }

  cityChange(event) {
    if (event.target.value !== 0) {
      this.setState({
        inputCity: event.target.value
      });
    } else {
      this.setState({
        inputCity: ''
      });
    }
  }

  changeCityHandler(e) {
    const data = e.target.value;
    if (e.target.value !== '') {
      const city = e.target.value.trim();
      this.setState({
        inputCity: city
      });
    } else {
      this.setState({
        inputCity: ''
      });
    }
    if (data !== '') {
      this.componentDidMount();
    } else {
      this.setState({
        locationData: {
          cityID: ''
        }
      });
      this.props.onSelectCity(this.state.locationData);
    }
  }

  componentDidMount() {
    axios
      .get('https://developers.zomato.com/api/v2.1/locations', {
        headers: { 'user-key': '4e02eb9035ae93ecffda5c28850e6442' },
        params: { query: this.state.inputCity }
      })
      .then(response => {
        if (response.data.location_suggestions.length !== 0) {
          const data = response.data.location_suggestions[0];
          this.setState({
            locationData: {
              cityID: data.city_id,
              entityID: data.entity_id,
              entityType: data.entity_type
            }
          });
        } else {
          this.setState({
            locationData: {
              cityID: '',
              entityID: '',
              entityType: ''
            }
          });
        }
        ////////////////////////////////////
        this.props.onSelectCity(this.state.locationData);
        //////////////////////////////////
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <div>
        <div className='input-group mb-3'>
          <input
            type='text'
            className='form-control'
            placeholder='Search for restaurants'
            aria-label="Recipient's username"
            aria-describedby='button-addon2'
            value={this.state.inputCity}
            onChange={this.cityChange.bind(this)}
          />
          <div className='input-group-append'>
            <button
              className='btn btn-outline-secondary'
              type='button'
              id='button-addon2'
              value={this.state.inputCity}
              onClick={this.changeCityHandler}
            >
              Button
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Search;
