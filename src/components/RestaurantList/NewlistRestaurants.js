import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import '../collectionList/collectionList.css';
import { fetchRestaurantsList } from '../../Store/Actions/restaurantListAction';

import img1 from '../../assets/img1.jpg'
import img2 from '../../assets/img2.jpg'
import img3 from '../../assets/img3.jpg'
import img4 from '../../assets/img4.jpg'
import img5 from '../../assets/img5.jpg'

const images=[
  img1,img2,img3,img4,img5
]

class NewlistRestaurants extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listRestaurant: [],
      entity_id: '',
      cuisinesData: [],
      establishmentData: [],
      searchRestaurantValue: '',
    };
    const entity_id = this.props.match.params.entity_id;
    this.setState({ entity_id: entity_id });
    const entity_type = this.props.match.params.entity_type;
    localStorage.setItem('listRestaurantEntityID', entity_id);
    localStorage.setItem('listRestaurantEntityTYPE', entity_type);

   
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.restList.data.restaurants) {
      this.setState({
        ...this.state,
        listRestaurant: nextProps.restList.data.restaurants
      });
      this.parseCuisines(nextProps.restList.data.restaurants);
      this.parseEstablisment(nextProps.restList.data.restaurants);
    } else {
      this.setState({ ...this.state, listRestaurant: [] });
    }
  }

  componentDidMount() {
    this.props.onListLoaded();
  }

  //////////parseCuisines Methods////////////////////////
  parseCuisines = (...array) => {
    if (array.length) {
      let duplicateCuisines = [];
      array.map((element, key) => {
        if (element.length) {
          element.map((e, key) => {
            if (e.restaurant.cuisines !== '') {
              duplicateCuisines.push(e.restaurant.cuisines);
            }
          });
        }
      });

      const uniqueArray = this.removeDuplicate(duplicateCuisines);
      this.setState({
        cuisinesData: uniqueArray
      });
    }
  };
  ///////////////////////////////////////////////////

  //////////parseEstablisment Methods////////////////////////
  parseEstablisment = (...array) => {
    if (array.length) {
      let duplicateestablisment = [];
      array.map((element, key) => {
        if (element.length) {
          element.map((e, key) => {
            if (e.restaurant.establishment.length !== 0) {
              duplicateestablisment.push(e.restaurant.establishment);
            }
          });
        }
      });

      const uniqueArray = this.removeDuplicate(duplicateestablisment);
      this.setState({
        establishmentData: uniqueArray
      });
    }
  };
  ///////////////////////////////////////////////////

  /////////////////Remove Duplicate/////////////////////
  removeDuplicate = (...duplicateArray) => {
    const array = [];
    let arrayToString = duplicateArray.toString();
    const data = arrayToString.split(',');
    for (let j = 0; j < data.length; j++) {
      array.push(data[j]);
    }

    let outputArray = [];
    let count = 0;
    let start = false;

    for (let j = 0; j < array.length; j++) {
      for (let k = 0; k < outputArray.length; k++) {
        if (array[j].trim() == outputArray[k].trim()) {
          start = true;
        }
      }
      count++;
      if (count == 1 && start == false) {
        outputArray.push(array[j].trim());
      }
      start = false;
      count = 0;
    }
    return outputArray;
  };
  //////////////////////////////////////////////////////

  ////////////////OnChange Cisines/////////////////////

  cuisinesDataSelected = event => {
    const value = event.target.value.trim();
    if (this.props.restList.data.restaurants.length !== 0) {
      const results = this.props.restList.data.restaurants.filter(e => {
        if (e.restaurant.cuisines != 0) {
          const data = e.restaurant.cuisines.trim();
          return data.includes(value);
        }
      });
      this.setState({
        ...this.state,
        listRestaurant: results
      });
    }
  };

  /////////////////////////////////////////////////////////////

  ///////////////OnChange Establishment/////////////////////

  establismentDataSelected = event => {
    const value = event.target.value.trim();
    if (this.props.restList.data.restaurants.length !== 0) {
      const results = this.props.restList.data.restaurants.filter(e => {
        if (e.restaurant.establishment.length) {
          const data = e.restaurant.establishment[0].trim();
          return e.restaurant.establishment[0].includes(value);
        }
      });
      this.setState({
        ...this.state,
        listRestaurant: results
      });
    }
  };

  /////////////////////////////////////////////////////////////

  ////////////////////////////////OnChange Ratings/////////////////////////////

  ratingsDataSelected = event => {
    const value = event.target.value.trim();
    if (this.props.restList.data.restaurants.length !== 0) {
      if (value === 'Ascending') {
        const dataList = this.props.restList.data.restaurants.sort(function(
          a,
          b
        ) {
          return (
            a.restaurant.user_rating.aggregate_rating -
            b.restaurant.user_rating.aggregate_rating
          );
        });
        this.setState({
          ...this.state,
          listRestaurant: dataList
        });
      } else if (value === 'Descending') {
        const dataList = this.props.restList.data.restaurants.sort(function(
          a,
          b
        ) {
          return (
            b.restaurant.user_rating.aggregate_rating -
            a.restaurant.user_rating.aggregate_rating
          );
        });
        this.setState({
          ...this.state,
          listRestaurant: dataList
        });
      }
    }
  };

  ////////////////////////End of ratingsDataSelected Selected///////////////////////

  ////////////////////////OnRestaurantSearch//////////////////////////////////////////
  handleInputChange = event => {
    const value = event.target.value.trim();
    if (this.props.restList.data.restaurants.length !== 0) {
      this.setState({
        searchRestaurantValue: value
      });
      const results = this.props.restList.data.restaurants.filter(e =>
        e.restaurant.name.toLowerCase().match(value.toLowerCase())
      );
      this.setState({
        ...this.state,
        listRestaurant: results
      });
    }
  };

  /////////////////////////////////////////////////////////////////////////////////////

  resetSearchNFilter = () => {
    if (this.props.restList.data.restaurants.length !== 0) {
      this.setState({
        ...this.state,
        listRestaurant: this.props.restList.data.restaurants
      });
    }
  };
  /////////////////////////////////////////////////////////////////////////////////////

  render() {
    const { error, loading, restList } = this.props;

    if (error) {
      return <div>Error! {error.message}</div>;
    }

    if (loading) {
      return <div>Loading...</div>;
    }
    return (
      <div>
        {/* ////////////////////Search Field//////////////////////// */}
        <div className='input-group mb-3'>
          <input
            type='text'
            className='form-control'
            placeholder='Search for restaurants'
            aria-label="Recipient's username"
            aria-describedby='button-addon2'
            onKeyDown={this.handleInputChange}
          />
          <div className='input-group-append'>
            <button
              className='btn btn-outline-secondary'
              type='button'
              id='button-addon2'
              onClick={this.resetSearchNFilter}
            >
              Reset
            </button>
          </div>
        </div>
        {/* ////////////////////Search Field END///////////////////// */}

        <div className='outerFilter'>
          <div className='filterDiv'>
            <div
              className='btn-group'
              role='group'
              aria-label='Button group with nested dropdown'
            >
              <select
                type='button'
                className='btn btn-secondary'
                onChange={this.cuisinesDataSelected}
              >
                <option disabled>Filter By Cuisines</option>
                {this.state.cuisinesData.length != 0 ? (
                  this.state.cuisinesData.map((e, key) => {
                    return (
                      <option key={key} value={e}>
                        {e}
                      </option>
                    );
                  })
                ) : (
                  <option value='No Data'>No Data</option>
                )}
              </select>
              {/* /////// */}
              <select
                type='button'
                className='btn btn-secondary'
                onChange={this.establismentDataSelected}
              >
                <option disabled>Filter By Establishment</option>
                {this.state.establishmentData.length != 0 ? (
                  this.state.establishmentData.map((e, key) => {
                    return (
                      <option key={key} value={e}>
                        {e}
                      </option>
                    );
                  })
                ) : (
                  <option value='No Data'>No Data</option>
                )}
              </select>
              {/* ///////////// */}
              <select
                type='button'
                className='btn btn-secondary'
                onChange={this.ratingsDataSelected}
              >
                <option disabled>Filter By Ratings</option>
                <option value='Ascending'>Ascending</option>
                <option value='Descending'>Descending</option>
              </select>
              {/* /////////////////////////// */}
            </div>
          </div>
        </div>

        {this.state.listRestaurant.length !== 0 ? (
          this.state.listRestaurant.map((e, key) => {
            return (
              <div className='rowCollection' key={key}>
                {e.restaurant.featured_image == '' ? (
                  <img
                    className='imageCollection flex-row'
                    src={images[Math.floor(Math.random() * images.length)]}
                    alt='collection'
                  />
                ) : e.restaurant.hasOwnProperty('featured_image') == false ? (
                  <img
                    className='imageCollection flex-row'
                    src={images[Math.floor(Math.random() * images.length)]}
                    alt='collection'
                  />
                ) : (
                  <img
                    className='imageCollection flex-row'
                    src={e.restaurant.featured_image}
                    alt='collection'
                  />
                )}
                <Link to={'/RestaurantDetails/' + e.restaurant.R.res_id}>
                  <div className='text-block'>
                    <h4>{e.restaurant.name}</h4>
                    <h6>
                      Ratings- {e.restaurant.user_rating.aggregate_rating}
                    </h6>
                  </div>
                </Link>
              </div>
            );
          })
        ) : (
          <div>
            <h5>
              No Restaurants please Go back to Home Page and Select Collections
            </h5>
            <Link to='/' exact>
              Home
            </Link>
          </div>
        )}
        {console.log('List SetState', this.state.listRestaurant)}
        {console.log('List REstaurants inside render', restList)}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    restList: state.RestaurantsList.restaurants,
    loading: state.RestaurantsList.loading,
    error: state.RestaurantsList.error
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onListLoaded: () => dispatch(fetchRestaurantsList())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewlistRestaurants);
