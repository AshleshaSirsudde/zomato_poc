import React from 'react'; 
import './Footer.css';


function Footer(){
    return (
      <div>
       <footer className="footerDiv">
       <p>Contact To : <a href="mailto:zomato@gmail.com">zomato@gmail.com</a></p>
       <small>By continuing past this page, you agree to our Terms of Service,
       2008-2020 &copy; Zomato™ Media Pvt Ltd. All rights reserved.</small>
       </footer>
      </div>
    );
}

export default Footer