import React,{Component} from 'react';


class ErrorBoundary extends Component {

    constructor(props){
        super(props)
        this.state ={
            hasError:false
        }
    }

    static getDerivedStateFromError(error)
     {
        return{
            hasError: true
        }
    }

    componentDidCatch(error, info){
        console.log("Error from componentDidCatch",error);
        console.log("Error Info from componentDidCatch",info);
    }

   render(){
       if(this.state.hasError){
           return<h1>Oops Something Went Wrong !!</h1>
       }
       return this.props.children  
   }
}

export default ErrorBoundary