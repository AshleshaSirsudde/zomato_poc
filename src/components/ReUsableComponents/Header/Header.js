import React from 'react'; 
import './Header.css';


function Header(){
    return (
      <div>
        <header className='inner-container flex-column'>
          <h1>Zomato</h1>
          <h3>Discover the best food and drinks</h3>
        </header>
      </div>
    );
}

export default Header