import React, { Component } from 'react';
import Search from '../Search/Search';
import axios from 'axios';
import { Link } from 'react-router-dom';
import './collectionList.css';

class CollectionList extends Component {
  constructor() {
    super();
    this.state = {
      cityID: '',
      entityID: '',
      entityType: '',
      collections: []
    };
  }

  handleCity = e => {
    if (e) {
      this.setState({
        cityID: e.cityID,
        entityID: e.entityID,
        entityType: e.entityType
      });
    }
    this.componentDidMount();
  };

  ////////////////////Did MOunt///////////////////////////
  componentDidMount() {
    if (this.state.cityID !== '') {
      axios
        .get('https://developers.zomato.com/api/v2.1/collections', {
          headers: { 'user-key': '4e02eb9035ae93ecffda5c28850e6442' },
          params: { city_id: this.state.cityID }
        })
        .then(response => {
          if (response.data.hasOwnProperty('collections')) {
            if (response.data.collections.length !== 0) {
              this.setState({
                collections: response.data.collections
              });
            } else {
              this.setState({ collections: [] });
            }
          } else {
            this.setState({ collections: [] });
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      this.setState({ collections: [] });
    }
  }
  ///////////////////////Did MOunt////////////////////////////

  render() {
    return (
      <div>
        <Search onSelectCity={this.handleCity}></Search>
        <h2>Collections</h2>
        <h5>
          Explore curated lists of top restaurants, cafes, pubs, and bars based
          on trends
        </h5>
        {this.state.collections.length === 0 ? (
          <h5 className='text-data'>No Data Found</h5>
        ) : (
          this.state.collections.map((e, key) => {
            return (
              <div className='rowCollection' key={key}>
                <Link
                  to={
                    '/NewlistRestaurants/' +
                    this.state.entityID +
                    '/' +
                    this.state.entityType
                  }
                >
                  <img
                    className='imageCollection flex-row'
                    src={e.collection.image_url}
                    alt='collection'
                  />
                  <div className='text-block'>
                    <h4>{e.collection.title}</h4>
                    <h6>No. of Res- {e.collection.res_count}</h6>
                  </div>
                </Link>
              </div>
            );
          })
        )}
      </div>
    );
  }
}

export default CollectionList;
