import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

function Review(props) {
  const [reviews, setReviews] = useState({});
  const [userReviews, setUserReviews] = useState([]);
  const res_id = props.match.params.res_id;
  const logeedName = localStorage.getItem('logeedName');

  //////////addReview/////////////////////////////////////////////
  const addReviewSelectorcomment = useSelector(
    state => state.AddReview.comments
  );
  const addReviewSelectorratings = useSelector(
    state => state.AddReview.ratings
  );
  ///////////////////////////////////////

  useEffect(() => {
    axios
      .get('https://developers.zomato.com/api/v2.1/reviews', {
        headers: { 'user-key': '4e02eb9035ae93ecffda5c28850e6442' },
        params: { res_id: res_id }
      })
      .then(response => {
        setReviews({ ...response.data });
        if (response.data.user_reviews) {
          setUserReviews(response.data.user_reviews);
        }
      });
  }, [res_id]);
  return (
    <div>
      <h3>Welcome {logeedName}</h3>
      <Link
        to={
          '/RestaurantDetails/' +
          res_id +
          '/Review/' +
          logeedName +
          '/AddReview'
        }
      >
        <button className='btn btn-secondary'>Add Reviews</button>
      </Link>
      <hr></hr>

      <div className='collections'>
        {userReviews.length === 0 ? (
          <h5 className='text-data'>No reviews Present</h5>
        ) : (
          userReviews.map((e, key) => {
            return (
              <div className='rowCollection' key={key}>
                {e.review.user.profile_image === '' ? (
                  <img
                    className='imageCollection flex-row'
                    src='https://b.zmtcdn.com/data/user_profile_pictures/359/024c287ab83af8cef25c65010fb04359.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A'
                    alt='collection'
                  />
                ) : (
                  <img
                    className='imageCollection flex-row'
                    src={e.review.user.profile_image}
                    alt='collection'
                  />
                )}
                <div className='text-block'>
                  {e.review.user.name === '' ? (
                    ''
                  ) : (
                    <h4>{e.review.user.name}</h4>
                  )}
                  {e.review.rating === '' ? (
                    ''
                  ) : (
                    <h6>Rating- {e.review.rating}</h6>
                  )}
                  {e.review.rating_text === '' ? (
                    ''
                  ) : (
                    <h6>Comments- {e.review.rating_text}</h6>
                  )}
                </div>
              </div>
            );
          })
        )}

        {/* ///////////////Added Review///////////////////////////// */}

        {addReviewSelectorratings !== '' &&
        (addReviewSelectorcomment !== undefined ||
          addReviewSelectorcomment !== '') ? (
          <div className='rowCollection'>
            <img
              className='imageCollection flex-row'
              src='https://b.zmtcdn.com/data/user_profile_pictures/359/024c287ab83af8cef25c65010fb04359.jpg?fit=around%7C100%3A100&crop=100%3A100%3B%2A%2C%2A'
              alt='collection'
            />
            <div className='text-block'>
              <h4>{logeedName}</h4>
              <h6>Rating- {addReviewSelectorratings}</h6>
              <h6>Comments- {addReviewSelectorcomment}</h6>
            </div>
          </div>
        ) : (
          <div></div>
        )}

        {/* ////////////////////////////////////////// */}
      </div>
    </div>
  );
}

export default Review;
