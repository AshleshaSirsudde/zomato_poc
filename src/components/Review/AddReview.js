import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import setAddReview from '../../Store/Actions/addReview';

function AddReview(props) {
  const [rating, setRating] = useState('');
  const [comment, setComment] = useState('');
  const name = props.match.params.name;
  const dispatch = useDispatch();

  const ratingChangeHandler = e => {
    const rating = e.target.value;
    setRating(rating);
  };

  const commentChangeHandler = e => {
    const comment = e.target.value;
    setComment(comment);
  };

  const submitHandler = e => {
    const ratingValue = rating;
    const commentValue = comment.trim();
    const payload = {
      rating: ratingValue,
      comment: commentValue
    };
    const getAddReview = () => dispatch(setAddReview(payload));
    getAddReview();

    if (ratingValue && commentValue) {
      props.history.push(
        '/RestaurantDetails/' + props.match.params.res_id + '/Review'
      );
    }
    e.preventDefault();
  };
  return (
    <div>
      <div className='container'>
        <div></div>
        <h3>Add Comments Here</h3>
        <form onSubmit={submitHandler}>
          <div className='form-group'>
            <label>Name:</label>
            <input
              disabled
              type='text'
              className='form-control'
              id='name'
              value={name}
              placeholder='name'
              name='name'
            />
          </div>
          <div className='form-group'>
            <label>Rating:</label>
            <input
              required
              type='number'
              min='1'
              max='5'
              className='form-control'
              id='num'
              value={rating}
              onChange={ratingChangeHandler}
              placeholder='Enter Rating'
              name='rating'
            />
          </div>
          <div className='form-group'>
            <label>Comments:</label>
            <input
              required
              type='text'
              className='form-control'
              id='comment'
              value={comment}
              onChange={commentChangeHandler}
              placeholder='Enter Comments'
              name='comment'
            />
          </div>
          <button type='submit' className='btn btn-primary'>
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}

export default AddReview;
