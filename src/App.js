import React from 'react';
import './App.css';

import { Route } from 'react-router-dom';
import Home from './components/Home/Home';
import RestaurantDetails from './components/RestaurantDetails/RestaurantDetails';
import Review from './components/Review/Review';
import Photos from './components/Photos/Photos';
import Login from './components/Login/Login';
import AddReview from './components/Review/AddReview';
import NewlistRestaurants from './components/RestaurantList/NewlistRestaurants';
import Header from './components/ReUsableComponents/Header/Header';
import Footer from './components/ReUsableComponents/Footer/Footer';
import ErrorBoundary from './components/ReUsableComponents/ErrorCatching/ErrorBoundary';

function App() {
  return (
    <div className='container'>
      <ErrorBoundary>
        <Header></Header>
      </ErrorBoundary>
      <div className='searchBar'>
        <ErrorBoundary>
          <Route path='/' exact component={Home} />
        </ErrorBoundary>
      </div>
      <ErrorBoundary>
        {' '}
        <Route
          path='/NewlistRestaurants/:entity_id/:entityType'
          exact
          component={NewlistRestaurants}
        />
      </ErrorBoundary>
      <ErrorBoundary>
        <Route
          path='/RestaurantDetails/:res_id'
          exact
          component={RestaurantDetails}
        />
      </ErrorBoundary>
      <ErrorBoundary>
        {' '}
        <Route
          path='/RestaurantDetails/:res_id/Login'
          exact
          component={Login}
        ></Route>
      </ErrorBoundary>
      <ErrorBoundary>
        {' '}
        <Route
          path='/RestaurantDetails/:res_id/Review'
          exact
          component={Review}
        ></Route>
      </ErrorBoundary>
      <ErrorBoundary>
        {' '}
        <Route
          path='/RestaurantDetails/:res_id/Review/:name/AddReview'
          exact
          component={AddReview}
        ></Route>
      </ErrorBoundary>
      <ErrorBoundary>
        {' '}
        <Footer></Footer>{' '}
      </ErrorBoundary>
    </div>
  );
}

export default App;
