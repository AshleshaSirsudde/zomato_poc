import * as actionTypes from './actionTypes';
import axios from 'axios';

const fetchCities = () => {
    return{
        type: actionTypes.FETCH_CITIES
    }
}

const fetchSuccess = cities => {
    return{
        type: actionTypes.FETCH_SUCCESS,
        payload: cities
    }
}

const fetchFailure = error => {
    return{
        type: actionTypes.FETCH_FAILURE,
        payload: error
    }
}

export const fetchCitiesList = () => {
    return (dispatch) => {
        dispatch(fetchCities)
        axios.get('https://indian-cities-api-nocbegfhqg.now.sh/cities')
        .then(response => {
            const cities = response.data
            dispatch(fetchSuccess(cities))
        })
        .catch(error => {
            dispatch(fetchFailure(error.message))
        })
    }
}

