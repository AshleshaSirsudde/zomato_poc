import * as actionTypes from './actionTypes';
import axios from 'axios';

const fetchLocations = () => {
  return {
    type: actionTypes.FETCH_LOCATION
  };
};

const fetchSuccess = locations => {
  return {
    type: actionTypes.FETCH_LOCATION_SUCCESS,
    payload: locations
  };
};

const fetchFailure = error => {
  return {
    type: actionTypes.FETCH_LOCATION_FAILURE,
    payload: error
  };
};

export const fetchLocationList = () => {
  const getCitiesName = localStorage.getItem('selectedCityData');

  console.log('getCitiesName', localStorage.getItem('selectedCityData'));
  return dispatch => {
    dispatch(fetchLocations);
    axios
      .get('https://developers.zomato.com/api/v2.1/locations', {
        headers: { 'user-key': '4e02eb9035ae93ecffda5c28850e6442' },
        params: { query: getCitiesName }
      })
      .then(response => {
        const locations = response.data;
        if (locations.location_suggestions) {
          locations.location_suggestions.map((e, key) => {
            const locationObj = {
              cityID: e.city_id,
              entityID: e.entity_id,
              entityType: e.entity_type
            };
            dispatch(fetchSuccess(locationObj));
          });
        }
      })
      .catch(error => {
        dispatch(fetchFailure(error.message));
      });
  };
};
