import * as actionTypes from './actionTypes';

 const setAddReview = (inputObj) =>{
    return{
    type : actionTypes.ADD_REVIEW,
    payload: inputObj
    }
}

export default setAddReview

