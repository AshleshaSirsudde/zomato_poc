import * as actionTypes from './actionTypes';
import axios from 'axios';

const fetchRestaurants = () => {
  return {
    type: actionTypes.FETCH_RESTAURANTS
  };
};

const fetchSuccess = restaurants => {
  return {
    type: actionTypes.FETCH_RESTAURANTS_SUCCESS,
    payload: restaurants
  };
};

const fetchFailure = error => {
  return {
    type: actionTypes.FETCH_RESTAURANTS_FAILURE,
    payload: error
  };
};

export const fetchRestaurantsList = () => {
 const entity_id = localStorage.getItem('listRestaurantEntityID');
  return dispatch => {
    dispatch(fetchRestaurants);
    axios
      .get('https://developers.zomato.com/api/v2.1/search', {
        headers: { 'user-key': '4e02eb9035ae93ecffda5c28850e6442' },
        params: { entity_id: entity_id }
      })
      .then(response => {
        const restaurants = response;
        dispatch(fetchSuccess(restaurants));
      })
      .catch(error => {
        dispatch(fetchFailure(error.message));
      });
  };
};
