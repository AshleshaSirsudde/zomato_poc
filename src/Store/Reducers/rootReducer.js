import {combineReducers} from 'redux';
import cityReducer from './cityReducer';
import locationReducer from './locationReducer';
import restaurantsReducer from './restaurantListReducer';
import addReview from './addReviewReducer';

const rootReducer = combineReducers({
       Cities : cityReducer,
       Locations: locationReducer,
       RestaurantsList: restaurantsReducer,
       AddReview: addReview
})

export default rootReducer