import {
    FETCH_CITIES,
    FETCH_SUCCESS,
    FETCH_FAILURE
} from '../Actions/actionTypes';

const initialState = { 
    loading: false,
    cities: [],
    error: ''
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case FETCH_CITIES:
           return{
                ...state,
               loading: true
           };
        case FETCH_SUCCESS:
            return{
                ...state,
                loading: false,
                cities: action.payload,
                error:''
            }
        case FETCH_FAILURE:
                return{
                    ...state,
                    loading: false,
                    cities:[],
                    error: action.payload
            }        
        default: return state;   
    }
 
}
export default reducer