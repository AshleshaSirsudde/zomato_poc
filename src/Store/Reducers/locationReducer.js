import {
  FETCH_LOCATION,
  FETCH_LOCATION_SUCCESS,
  FETCH_LOCATION_FAILURE
} from '../Actions/actionTypes';

const initialState = {
  loading: false,
  locations: [],
  cityId: 0,
  error: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LOCATION:
      return {
        ...state,
        loading: true
      };
    case FETCH_LOCATION_SUCCESS:
      return {
        ...state,
        loading: false,
        locations: action.payload,
        cityId: action.payload,
        error: ''
      };
    case FETCH_LOCATION_FAILURE:
      return {
        ...state,
        loading: false,
        locations: [],
        cityId: 0,
        error: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
