import {
    ADD_REVIEW
} from '../Actions/actionTypes';

const initialState = { 
    comments:'',
    ratings: ''
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case ADD_REVIEW:
           return{
                ...state,
               comments: action.payload.comment,
               ratings: action.payload.rating
           };
        default: return state;   
    }
 
}
export default reducer